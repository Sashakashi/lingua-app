package by.itstep.linguaapp.repository;
import by.itstep.linguaapp.entity.AnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Integer> {

    Optional<AnswerEntity> findByIdAndQuestionId(Integer answerId, Integer questionId);


}
