package by.itstep.linguaapp.dto.answer;
import lombok.Data;

@Data
public class AnswerFullDto {

    private Integer id;

    private String body;

    private Boolean correct;
}
