package by.itstep.linguaapp.dto.question;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Data;
import java.util.List;

@Data
public class QuestionShortDto {

    private Integer id;

    private String description;

    private QuestionLevel level;

    private List<CategoryFullDto> categories;
}
