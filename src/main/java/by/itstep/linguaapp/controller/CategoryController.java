package by.itstep.linguaapp.controller;
import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories/{id}")
    public CategoryFullDto findById(Integer id){
        return categoryService.findById(id);
    }

    @GetMapping("/categories")
    public List<CategoryFullDto> findAll(){
        return categoryService.findAll();
    }

    @PostMapping("/categories")
    public CategoryFullDto create(@Valid @RequestBody CategoryCreateDto dto){
        return categoryService.create(dto);
    }

    @PutMapping("/categories")
    public CategoryFullDto update(@Valid @RequestBody CategoryUpdateDto dto){
        return categoryService.update(dto);
    }

    @DeleteMapping("/categories/{id}")
    public void delete (Integer id){
        categoryService.delete(id);
    }


}
