package by.itstep.linguaapp.controller;
import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id){
      return userService.findById(id);
    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size){
        return userService.findAll(page, size);
    }

    @PostMapping("/users")
    public UserFullDto create(@Valid @RequestBody UserCreateDto dto){
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@Valid @RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @PutMapping("/users/password")
    public void changePassword(@Valid @RequestBody ChangeUserPasswordDto dto){
        userService.changePassword(dto);
    }

    @PutMapping("/users/{id}/block")
    public void block(@PathVariable Integer id, @RequestHeader("Authorization") String authorization) throws IOException {
        userService.block(id);
    }

    @PutMapping("/users/role")
    public UserFullDto changeRole(@Valid @RequestBody ChangeUserRoleDto dto){
        return userService.changeRole(dto);
    }



}
