package by.itstep.linguaapp.exception;

public class WrongUserPasswordException extends RuntimeException {

    public WrongUserPasswordException(String message){
        super(message);
    }
}
