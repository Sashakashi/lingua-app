package by.itstep.linguaapp.security;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public UserEntity getAuthenticatedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(auth == null){
            return null;
        }

        return userRepository.findByEmail(auth.getName()).orElseThrow();
    }
}
