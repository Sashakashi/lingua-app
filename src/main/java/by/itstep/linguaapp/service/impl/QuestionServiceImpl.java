package by.itstep.linguaapp.service.impl;
import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.MailService;
import by.itstep.linguaapp.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {


    private final QuestionRepository questionRepository;
    private final QuestionMapper questionMapper;
    private final CategoryRepository categoryRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;
    private final MailService mailService;

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto createDto) {
        throwIfInvalidNumberOfCorrectAnswers(createDto);
        QuestionEntity questionToSave = questionMapper.map(createDto);
        for (AnswerEntity answer : questionToSave.getAnswers()) {
            answer.setQuestion(questionToSave);
        }
        List<CategoryEntity> categoriesToAdd = categoryRepository.findAllById(createDto.getCategoriesId());
        questionToSave.setCategories(categoriesToAdd);
        QuestionEntity savedQuestion = questionRepository.save(questionToSave);
        QuestionFullDto questionDto = questionMapper.map(savedQuestion);
        log.info("Question was successfully created.");
        return questionDto;
    }


    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto updateDto) {

        QuestionEntity entityToUpdate = questionRepository.findById(updateDto.getId())
                .orElseThrow(()->new AppEntityNotFoundException("QuestionEntity was not found by id: " + updateDto.getId()));

        entityToUpdate.setDescription(updateDto.getDescription());
        entityToUpdate.setLevel(updateDto.getLevel());
        QuestionEntity updatedEntity = questionRepository.save(entityToUpdate);
        QuestionFullDto updatedDto = questionMapper.map(updatedEntity);
        log.info("Question was successfully updated.");
        return updatedDto;
    }

    @Override
    @Transactional
    public QuestionFullDto findById(Integer id) {

        QuestionEntity foundEntity = questionRepository.findById(id)
                .orElseThrow(()->new AppEntityNotFoundException("QuestionEntity was not found by id: " + id));

        QuestionFullDto foundDto = questionMapper.map(foundEntity);
        log.info("Question was successfully found.");
        return foundDto;
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {
        List<QuestionEntity> foundEntities = questionRepository.findAll();
        List<QuestionShortDto> foundDtos = questionMapper.map(foundEntities);
        log.info(foundDtos.size() + " questions were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {

        QuestionEntity entityToDelete = questionRepository.findById(id)
                .orElseThrow(()->new AppEntityNotFoundException("QuestionEntity was not found by id: " + id));

        entityToDelete.setDeletedAt(Instant.now());
        for(AnswerEntity answer : entityToDelete.getAnswers()){
            answer.setDeletedAt(Instant.now());
        }
        questionRepository.save(entityToDelete);
        log.info("Question was successfully deleted.");
    }

    @Override
    @Transactional
    public boolean checkAnswer(Integer questionId, Integer answerId) {

        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId)
                .orElseThrow(()->new AppEntityNotFoundException("Answer was not found by id: " + answerId +
                        " in the question " + questionId));

        UserEntity user = authenticationService.getAuthenticatedUser();
        user.setLastAnswerDate(Instant.now());
        userRepository.save(user);

        if (answer.getCorrect()) {
            QuestionEntity question = answer.getQuestion();
            question.getUsersWhoCompleted().add(user);
            questionRepository.save(question);
            mailService.sendEmail(user.getEmail(), "Good jop! Right answer!");
        }
        return answer.getCorrect();
    }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {
        UserEntity user = authenticationService.getAuthenticatedUser();
        List<QuestionEntity> foundQuestions = questionRepository.findNotCompleted(categoryId, user.getId());
        if (foundQuestions.isEmpty()) {
            throw new AppEntityNotFoundException("Can not find available questions by category id: " + categoryId);
        }
        int randomIndex = (int) (foundQuestions.size() * Math.random());
        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        log.info("Random question was successfully found.");
        return questionMapper.map(randomQuestion);
    }


    private void throwIfInvalidNumberOfCorrectAnswers(QuestionCreateDto createDto) {
        int counter = 0;
        for (AnswerCreateDto answer : createDto.getAnswers()) {
            if (answer.getCorrect()) {
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contain the only one correct answer.");
        }
    }


}
