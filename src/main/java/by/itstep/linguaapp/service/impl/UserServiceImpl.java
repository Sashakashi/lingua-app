package by.itstep.linguaapp.service.impl;
import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValueIsTakenException;
import by.itstep.linguaapp.exception.WrongUserPasswordException;
import by.itstep.linguaapp.mapper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToSave = userMapper.map(createDto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);
        entityToSave.setPassword(passwordEncoder.encode(createDto.getPassword()+createDto.getEmail()));

        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(createDto.getEmail());
        if(entityWithSameEmail.isPresent()){
            throw new UniqueValueIsTakenException("Email is taken!");
        }

        UserEntity savedEntity = userRepository.save(entityToSave);
        UserFullDto createdDto = userMapper.map(savedEntity);
        log.info("User was successfully created.");
        return createdDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto updateDto) {
        UserEntity entityToUpdate = userRepository.findById(updateDto.getId()).
                orElseThrow(()-> new AppEntityNotFoundException("UserEntity was not found by id: " + updateDto.getId()));

        throwIfNotCurrentUserOrAdmin(entityToUpdate.getEmail());

        entityToUpdate.setCountry(updateDto.getCountry());
        entityToUpdate.setPhone(updateDto.getPhone());
        entityToUpdate.setName(updateDto.getName());

        UserEntity updatedUser = userRepository.save(entityToUpdate);
        UserFullDto savedDto = userMapper.map(updatedUser);
        log.info("User was successfully updated.");
        return savedDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserEntity foundEntity = userRepository.findById(id).
                orElseThrow(()-> new AppEntityNotFoundException("UserEntity was not found by id: " + id));
        UserFullDto userDto = userMapper.map(foundEntity);
        log.info("User was successfully found.");
        return userDto;
    }

    @Override
    @Transactional
    public Page<UserFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page,size);

        Page<UserEntity> foundEntities = userRepository.findAll(pageable);
        Page<UserFullDto> foundDtos = foundEntities.map(entity -> userMapper.map(entity));
        log.info(foundDtos.getNumberOfElements() + " users were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(int id) {

        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);
        log.info("User was successfully deleted.");
    }

    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {

        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!userToUpdate.getPassword().equals(passwordEncoder.encode(dto.getOldPassword()+userToUpdate.getEmail()))) {
            throw new WrongUserPasswordException("Wrong password.");
        }
        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword()+userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        log.info("User password was successfully updated.");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeUserRoleDto dto) {

        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        userToUpdate.setRole(dto.getNewRole());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);
        log.info("User role was successfully updated.");
        return userDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {

        UserEntity entityToUpdate = userRepository.findById(userId)
                .orElseThrow(()->new AppEntityNotFoundException("UserEntity was not found by id: " + userId));

        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        log.info("User was successfully updated.");
    }

    private boolean currentUserIsAdmin() {
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_" + UserRole.ADMIN.name()));
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}
