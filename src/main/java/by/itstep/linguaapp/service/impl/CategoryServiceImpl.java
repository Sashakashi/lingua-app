package by.itstep.linguaapp.service.impl;
import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValueIsTakenException;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {


    private final CategoryMapper categoryMapper;
    private final CategoryRepository categoryRepository;


    @Override
    @Transactional
    public CategoryFullDto create(CategoryCreateDto createDto) {
        Optional<CategoryEntity> foundEntity = categoryRepository.findByName(createDto.getName());
         if(foundEntity.isPresent()){
            throw new UniqueValueIsTakenException("Category name is taken!");
        }
        CategoryEntity entityToSave = categoryMapper.map(createDto);
        CategoryEntity savedEntity = categoryRepository.save(entityToSave);
        CategoryFullDto createdDto = categoryMapper.map(savedEntity);
        log.info("Category was successfully created.");
        return createdDto;
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto updateDto) {

        CategoryEntity foundEntity = categoryRepository.findById(updateDto.getId())
                .orElseThrow(()->new AppEntityNotFoundException("Category was not found by id: "+updateDto.getId()));

        CategoryEntity entityToUpdate = categoryRepository.getById(updateDto.getId());
        entityToUpdate.setName(updateDto.getName());
        CategoryEntity savedEntity = categoryRepository.save(entityToUpdate);
        CategoryFullDto savedDto = categoryMapper.map(savedEntity);
        log.info("Category was successfully updated.");
        return savedDto;
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {

        CategoryEntity foundEntity = categoryRepository.findById(id)
                .orElseThrow(()->new AppEntityNotFoundException("CategoryEntity was not found by id: "+id));

        CategoryFullDto foundDto = categoryMapper.map(foundEntity);
        log.info("Category was successfully found.");
        return foundDto;
    }

    @Override
    @Transactional
    public List<CategoryFullDto> findAll() {
        List<CategoryEntity> foundEntities = categoryRepository.findAll();
        List<CategoryFullDto> foundDtos = categoryMapper.map(foundEntities);
        log.info(foundDtos.size() + " categories were found.");
        return foundDtos;
    }

    @Override
    @Transactional
    public void delete(int id) {

        CategoryEntity entityToDelete = categoryRepository.findById(id)
                .orElseThrow(()-> new AppEntityNotFoundException("CategoryEntity was not found by id: "+id));

        entityToDelete.setDeletedAt(Instant.now());
        categoryRepository.save(entityToDelete);
        log.info("category was successfully deleted.");
    }
}
