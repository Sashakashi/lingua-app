package by.itstep.linguaapp.service.impl;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.BusinessLogicException;
import by.itstep.linguaapp.mapper.AnswerMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.service.AnswerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;

@Service
@Slf4j
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {


    private final AnswerRepository answerRepository;
    private final AnswerMapper answerMapper;

    @Override
    @Transactional
    public AnswerFullDto update(AnswerUpdateDto updateDto) {

        AnswerEntity answerToUpdate = answerRepository.findById(updateDto.getId())
                .orElseThrow(()->new AppEntityNotFoundException("Answer was not found by id: "+updateDto.getId()));

        if(!updateDto.getCorrect() && answerToUpdate.getCorrect()){
            throw new BusinessLogicException("Current answer is correct. Set other answer as correct before the update.");
        }
        if(updateDto.getCorrect() && !answerToUpdate.getCorrect()){
            removeCorrectFlagFromAnswers(answerToUpdate.getQuestion());
            }
        answerToUpdate.setCorrect(updateDto.getCorrect());
        answerToUpdate.setBody(updateDto.getBody());
        AnswerEntity updatedAnswer = answerRepository.save(answerToUpdate);
        log.info("Answer was successfully updated.");
        return answerMapper.map(updatedAnswer);
    }

    @Override
    @Transactional
    public void delete(Integer id) {

        AnswerEntity entityToDelete = answerRepository.findById(id)
                .orElseThrow(()->new AppEntityNotFoundException("Answer was not found by id: "+id));

        if(entityToDelete.getCorrect()){
            throw new BusinessLogicException("Current answer is correct. Set other answer as correct before the delete.");
        }
        if(entityToDelete.getQuestion().getAnswers().size()<3){
            throw new BusinessLogicException("Question must contain at least 2 answers.");
        }
        entityToDelete.setDeletedAt(Instant.now());
        answerRepository.save(entityToDelete);
    }

    private void removeCorrectFlagFromAnswers(QuestionEntity question){
        question.getAnswers().stream()
                .filter(answer-> answer.getCorrect())
                .peek(answer->answer.setCorrect(false))
                .forEach(answer->answerRepository.save(answer));
    }


}
