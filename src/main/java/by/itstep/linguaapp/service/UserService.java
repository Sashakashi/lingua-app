package by.itstep.linguaapp.service;
import by.itstep.linguaapp.dto.user.*;
import org.springframework.data.domain.Page;


public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    Page<UserFullDto> findAll(int page, int size);

    void delete(int id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void block(Integer userId);


}
