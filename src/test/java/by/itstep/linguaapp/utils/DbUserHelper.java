package by.itstep.linguaapp.utils;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import static by.itstep.linguaapp.LinguaappApplicationTests.FAKER;

@Component
public class DbUserHelper {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEntity addUserToDb(UserRole role){
        UserEntity user = new UserEntity();
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(passwordEncoder.encode(FAKER.internet().password()+user.getEmail()));
        user.setRole(role);
        user.setName(FAKER.name().firstName());
        user.setCountry(FAKER.country().name());
        user.setBlocked(false);
        user.setPhone(FAKER.phoneNumber().cellPhone());

        return userRepository.save(user);
    }
}
