package by.itstep.linguaapp.mapper;
import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToDto_happyPath(){
        //given
        UserEntity entity = new UserEntity();
        entity.setId(15);
        entity.setPhone("123 654 22");
        entity.setRole(UserRole.ADMIN);
        entity.setName("Sasha");
        entity.setCountry("Belarus");
        entity.setEmail("sa@mail.ru");
        entity.setPassword("123");
        //when
        UserFullDto dto = userMapper.map(entity);
        //then
        Assertions.assertNotNull(dto);
        System.out.println("->"+dto);
    }

}
