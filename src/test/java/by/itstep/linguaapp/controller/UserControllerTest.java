package by.itstep.linguaapp.controller;
import by.itstep.linguaapp.LinguaappApplicationTests;
import by.itstep.linguaapp.dto.user.UserCreateDto;
import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.utils.DbUserHelper;
import by.itstep.linguaapp.utils.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static by.itstep.linguaapp.utils.DtoUserHelper.generateUserCreateDto;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest extends LinguaappApplicationTests {

    @Autowired
    private DbUserHelper dbUserHelper;

    @Autowired
    private JwtHelper jwtHelper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findById_happyPath()throws Exception{
        //given
        UserEntity existingUser = dbUserHelper.addUserToDb(UserRole.USER);
        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        //when
        //mockMvc.perform(MockMvcRequestBuilders.request(HttpMethod.GET,"/users/" + existingUser.getId()));
        MvcResult mvcResult = mockMvc.perform(request(GET,"/users/" + existingUser.getId())
                .header("Authorization",token))
                .andExpect(status().isOk())
                .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertEquals(existingUser.getId(), foundUser.getId());
    }

    @Test
    public void findById_whenNotAuthenticated()throws Exception{
        //given
        UserEntity existingUser = dbUserHelper.addUserToDb(UserRole.USER);
        //when
        mockMvc.perform(request(GET,"/users/" + existingUser.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void create_happyPath()throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDto createDto = generateUserCreateDto();
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/users")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                        .andExpect(status().isOk())
                        .andReturn();

        byte [] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);
        //then
        Assertions.assertNotNull(foundUser.getId());
        Assertions.assertEquals(UserRole.USER, foundUser.getRole());
    }

    @Test
    public void create_whenNotAuthenticated()throws Exception {
        //given
        UserCreateDto createDto = generateUserCreateDto();
        //when
        mockMvc.perform(request(POST, "/users")
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                        .andExpect(status().isForbidden());

    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dbUserHelper.addUserToDb(UserRole.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDto createDto = generateUserCreateDto();

        //when
        mockMvc.perform(request(POST, "/users")
                        .header("Authorization", token)
                        .header("Content-Type", "application/json")
                        .content(objectMapper.writeValueAsString(createDto)))
                        .andExpect(status().isForbidden());
    }








}
