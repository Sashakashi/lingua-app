package by.itstep.linguaapp.service;
import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.entity.*;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class QuestionServiceTest {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private UserRepository userRepository;
    @MockBean
    private AuthenticationService authenticationService;


    @BeforeEach
    public void setUp() {
        answerRepository.deleteAllInBatch();
        questionRepository.deleteAllInBatch();
        categoryRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @Test
    public void findAll_happyPath(){
        //given
        CategoryEntity category = addCategoryToDB();
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());
        addQuestionToDb(category.getId());

        QuestionFullDto question = addQuestionToDb(category.getId());
        questionService.delete(question.getId());
        //when
        List<QuestionShortDto> foundQuestions = questionService.findAll();
        //then
        Assertions.assertEquals(5,foundQuestions.size());
    }

    @Test
    @Transactional
    public void create_happyPath() {
        //given
        CategoryEntity existingCategory = addCategoryToDB();
        List<Integer> categoryIds = Arrays.asList(existingCategory.getId());
        QuestionCreateDto createDto = generateQuestionCreateDto(categoryIds);
        //when
        QuestionFullDto created = questionService.create(createDto);
        //then
        Assertions.assertNotNull(created);
        Assertions.assertNotNull(created.getId());
        QuestionEntity createdEntity = questionRepository.getById(created.getId());
        Assertions.assertEquals(createdEntity.getAnswers().size(), createDto.getAnswers().size());
        for (AnswerEntity savedAnswer : createdEntity.getAnswers()) {
            Assertions.assertNotNull(savedAnswer.getId());
        }
        //Assertions.assertEquals(createdEntity.getCategories().size(), createDto.getCategoriesId().size());
    }

    private QuestionCreateDto generateQuestionCreateDto(List<Integer> categoryIds) {
        QuestionCreateDto questionCreateDto = new QuestionCreateDto();
        questionCreateDto.setDescription("test-description");
        questionCreateDto.setLevel(QuestionLevel.A1);
        questionCreateDto.setCategoriesId(categoryIds);

        List<AnswerCreateDto> answers = Arrays.asList(
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(true),
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(false)
        );
        questionCreateDto.setAnswers(answers);
        return questionCreateDto;
    }

    @Test
    @Transactional
    public void getRandomQuestion_happyPath() {
        //given
        CategoryEntity firstCategory = addCategoryToDB();
        CategoryEntity secondCategory = addCategoryToDB();
        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());
        QuestionFullDto secondQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());
        UserEntity user = addUserToDb();
        Mockito.when(authenticationService.getAuthenticatedUser()).thenReturn(user);
        //when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId());
        //then
        Assertions.assertEquals(firstQuestion.getId(), foundQuestion.getId());
    }

    @Test
    @Transactional
    public void getRandomQuestion_whenOneQuestionCompleted() {
        //given
        CategoryEntity firstCategory = addCategoryToDB();
        CategoryEntity secondCategory = addCategoryToDB();
        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());
        QuestionFullDto secondQuestion = addQuestionToDb(firstCategory.getId());
        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto fourthQuestion = addQuestionToDb(secondCategory.getId());
        UserEntity user = addUserToDb();
        Integer correctAnswerId = null;
        for (AnswerFullDto answer : secondQuestion.getAnswers()) {
            if (answer.getCorrect()) {
                correctAnswerId = answer.getId();
            }
        }

        Mockito.when(authenticationService.getAuthenticatedUser()).thenReturn(user);
        questionService.checkAnswer(secondQuestion.getId(), correctAnswerId);
        //when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId());
        //then
        Assertions.assertEquals(firstQuestion.getId(), foundQuestion.getId());
    }

    private QuestionFullDto addQuestionToDb(Integer categoryId) {
        QuestionCreateDto createDto = generateQuestionCreateDto(Arrays.asList(categoryId));
        return questionService.create(createDto);
    }

    private AnswerCreateDto generateAnswerCreateDto(boolean correct) {
        AnswerCreateDto answerCreateDto = new AnswerCreateDto();
        answerCreateDto.setBody("test-body");
        answerCreateDto.setCorrect(correct);
        return answerCreateDto;
    }

    private CategoryEntity addCategoryToDB() {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName("test-name");
        return categoryRepository.save(categoryEntity);
    }

    private UserEntity addUserToDb() {
        UserEntity user = new UserEntity();
        user.setBlocked(false);
        user.setPassword("test-password");
        user.setCountry("test-country");
        user.setRole(UserRole.USER);
        user.setName("Bob");
        user.setPhone("test-phone");
        user.setEmail("test-email");
        return userRepository.save(user);
    }


}
